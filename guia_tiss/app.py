# -*- coding: utf-8 -*-
from flask import Flask, render_template, json, Response
import os
import xmltodict

PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))
app = Flask(__name__)

OBJECTS, TIPO_XML = None, None
PROCEDIMENTOS, EQUIPES = {}, {}

def load_procedimentos_servicos():
    '''
    É declarado as variáveis globais do sistema: OBJECTS, PROCEDIMENTOS, TIPO_XML, TIPO_EQUIPE
    OBJECTS: Já contendo os dados do XML, no formato JSON, é utilizado para definir o tipo da guia
    TIPO_XML: Já possuí o nome da tag com o tipo de guia
    TIPO_EQUIPE: Já possuí o nome da tag com o tipo equipe
    PROCEDIMENTOS: É um dicioário com a seguinte estrutura:
        PROCEDIMENTOS = { 'número da guia': {
                'dados' : Recebe a lista dos procedimentos executados na guia
                'equipes': Recebe a lista das equipes envolvida no procedimento
            }  
    '''
    global OBJECTS
    global PROCEDIMENTOS
    global TIPO_XML
    global TIPO_EQUIPE

    try:
        if OBJECTS['mensagemTISS']['prestadorParaOperadora']['loteGuias']['guiasTISS'][TIPO_XML]['procedimentosExecutados']:
            try:
                equipes = []
                try:
                    equipes = OBJECTS['mensagemTISS']['prestadorParaOperadora']['loteGuias']['guiasTISS'][TIPO_XML]['procedimentosExecutados']['procedimentoExecutado'][TIPO_EQUIPE]
                except:
                    pass
    
                PROCEDIMENTOS.update({ 
                    OBJECTS['mensagemTISS']['prestadorParaOperadora']['loteGuias']['guiasTISS'][TIPO_XML]['cabecalhoGuia']['numeroGuiaPrestador'] : {
                        'dados' : OBJECTS['mensagemTISS']['prestadorParaOperadora']['loteGuias']['guiasTISS'][TIPO_XML]['procedimentosExecutados']['procedimentoExecutado'],
                        'equipes' : equipes
                    }
                })
            except:
                PROCEDIMENTOS.update({ 
                    OBJECTS['mensagemTISS']['prestadorParaOperadora']['loteGuias']['guiasTISS'][TIPO_XML]['cabecalhoGuia']['numeroGuiaPrestador'] : { 
                        'dados' : [],
                        'equipes' : []
                    }
                })
    except:
        for guia in OBJECTS['mensagemTISS']['prestadorParaOperadora']['loteGuias']['guiasTISS'][TIPO_XML]:
            try:
                equipes = []
                try:
                    equipes = guia['procedimentosExecutados']['procedimentoExecutado'][TIPO_EQUIPE]
                except:
                    pass
                
                PROCEDIMENTOS.update({ 
                    guia['cabecalhoGuia']['numeroGuiaPrestador'] : {
                        'dados' : guia['procedimentosExecutados']['procedimentoExecutado'],
                        'equipes' : equipes
                    }
                })                
            except:
                pass
                # PROCEDIMENTOS.update({ 
                #     guia['cabecalhoGuia']['numeroGuiaPrestador'] : { 
                #         'dados' : [],
                #         'equipes' : []
                #     }
                # })       

@app.template_filter('formatar_data')
def formatar_data(data):
    '''
    Formata a data do padrão americano (ano-med-dia), para o padrão brasileiro (dia-mes-ano)
    '''
    if data:
        data_split = data.split('-')
        return data_split[2]+'/'+data_split[1]+'/'+data_split[0]
    return ''

@app.template_filter('procedimentos')
def procedimentos(nr_guia):
    '''
    Recebe como parâmetro o Número da Guia e recupera os dados dos PROCEDIMENTOS
    onde 'dados' é a lista com os procedimentos carregados do arquivo
    '''
    global PROCEDIMENTOS
    return PROCEDIMENTOS[nr_guia]['dados']

@app.template_filter('reset_equipes')
def reset_equipes(nr_guia):
    '''
    Para cada Guia é removido todos os dados populados na EQUIPE, pois para cada
    guia que é renderizado, é adicionado equipes na variável EQUIPES pela função load_equipes
    '''
    global EQUIPES
    EQUIPES = {}
    return ''

@app.template_filter('load_equipes')
def load_equipes(equipes, index, nr_guia):
    '''
    Na variável EQUIPES, é criado (adicionado) uma posição com o Número da Guia contendo
    um dicioário, onde:
    'equipes' é o nó do xml onde contem a lista das equipes do procedimento
    que está sendo impresso.
    'nr_procedimento' é o índice do procedimento, essa informação é auto-incrementada para 
    cada procedimento que é impresso na tela.
    '''
    global EQUIPES

    try:
        EQUIPES[nr_guia].append({
            'dados' : equipes ,
            'nr_procedimento' : index
        })
    except:
        EQUIPES.update({ 
            nr_guia : [{
                'dados' : equipes ,
                'nr_procedimento' : index
            }]
        })
    return ''

@app.template_filter('equipes')
def equipes(nr_guia):
    '''
    Retorna uma lista contendo a da guia que está sendo passada como parâmetro
    '''
    global EQUIPES
    try:
        return EQUIPES[nr_guia] 
    except:
        return []

def read_xml(nome_arquivo):
    '''
    Recebe o nome do arquivo xml à ser lido. O arquivo é lido e convertido para o 
    formato de JSON.
    Para identifica o tipo do arquivo, é feito uma tentativa de acessar o nó 'guiaSP-SADT':
    - Caso consiga, o 'tipo' da guia é de serviço 'spsadt'. É definido os valores nas variáveis 
    TIPO_XML com o nome da tag 'guiaSP-SADT' e TIPO_EQUIPE com o nome da tag 'equipeSadt'
    - Caso não consiga, é feito uma tentativa de acessar o nó 'guiaResumoInternacao':
    Caso consiga, o 'tipo' da guia é de serviço 'resumo'. É definido os valores nas variáveis 
    TIPO_XML com o nome da tag 'guiaResumoInternacao' e TIPO_EQUIPE com o nome da tag 'identEquipe'
    - Caso não consiga, o 'tipo', TIPO_XML e TIPO_EQUIPE são definidos como 'None'
    '''
    _return = {
        'tipo' : '',
        'objects' : ''
    }
    try:
        namespaces = {'ans':None}
        arquivo = open(os.path.join(PROJECT_DIR+'/xml/', nome_arquivo))
        xml = xmltodict.parse(arquivo, namespaces=namespaces)
        arquivo.close()
        
        global OBJECTS
        global TIPO_XML
        global TIPO_EQUIPE
        global UNICA
        OBJECTS = json.loads(json.dumps(xml))
        UNICA   = False

        try:
            if OBJECTS['mensagemTISS']['prestadorParaOperadora']['loteGuias']['guiasTISS']['guiaSP-SADT']:
                _return['tipo'] = 'spsadt'
                TIPO_XML = 'guiaSP-SADT'
                TIPO_EQUIPE = 'equipeSadt'
        except Exception, e:
            try:
                if OBJECTS['mensagemTISS']['prestadorParaOperadora']['loteGuias']['guiasTISS']['guiaResumoInternacao']:
                    _return['tipo'] = 'resumo'
                    TIPO_XML = 'guiaResumoInternacao'
                    TIPO_EQUIPE = 'identEquipe'
            except Exception, e:
                _return['tipo'] = None
                TIPO_XML = None
                TIPO_EQUIPE = None

        try:
            if OBJECTS['mensagemTISS']['prestadorParaOperadora']['loteGuias']['guiasTISS'][TIPO_XML]['cabecalhoGuia']['numeroGuiaPrestador']:
                UNICA = True
        except:
            pass
            
        _return['objects'] = OBJECTS
    except Exception as e:
        _return['tipo'] = 'erro'
        _return['msg'] = u'Erro ao ler o arquivo: %s - %s' % (nome_arquivo, e)
    return _return

@app.route("/<arquivo_xml>")
def home(arquivo_xml=None):
    '''
    Recebe como parâmetro o nome do arquivo que será renderizado para impressão.
    Utiliza a função read_xml() para realizar a leitura do arquivo, onde a mesma 
    retorna um dicionário.
    - Caso o 'tipo' do retorno seja 'erro', é exibido no browser a mensagem de erro ao
    ler o arquivo.
    - Caso o 'tipo' do retorno seja 'resumo', é renderizado o template guia_resumo_internacao.html
    passando o parâmetro o 'titulo' da guia e o 'objects' que contem os dados do XML no formato JSON.
    - Caso o 'tipo' do retorno seja 'spsadt', é renderizado o template guia_servico_spsadt.html
    passando o parâmetro o 'titulo' da guia e o 'objects' que contem os dados do XML no formato JSON.
    - Caso o 'tipo' do retorno não seja nenhuma das opções anteriores, é exibido no browser uma mensagem
    notificando que o tipo da guia não é válido.
    Se o 'tipo' for de 'resumo' ou 'spsadt' é chamado a função load_procedimentos_servicos()
    '''
    try:
        xml = read_xml(arquivo_xml)

        if xml['tipo']=='erro':
            return u'%s' % xml['msg']

        load_procedimentos_servicos()
        
        global UNICA
        global TIPO_XML
        if UNICA:
            objs = [xml['objects']['mensagemTISS']['prestadorParaOperadora']['loteGuias']['guiasTISS'][TIPO_XML]]
        else:
            objs = xml['objects']['mensagemTISS']['prestadorParaOperadora']['loteGuias']['guiasTISS'][TIPO_XML]
        
        if xml['tipo']=='resumo':  
            return render_template('guia_resumo_internacao.html', titulo=u'GUIA RESUMO DE INTERNAÇÃO', objects=objs)
        elif xml['tipo']=='spsadt':
            return render_template('guia_servico_spsadt.html', titulo=u'GUIA DE SERVIÇO PROFISSIONAL / SERVIÇO AUXILIAR DE DIAGNÓSTICO E TERAPIA - SP/SADT', objects=objs)
        else:    
            return u'Guia inválida. %s' % xml['tipo']
    except Exception, e:
        return u'Erro ao renderizar o XML. Verifique: %s' % e.message


if __name__ == "__main__":
    app.debug = True
    app.run()